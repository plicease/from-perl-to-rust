# Generic Types

Rust also supports the notion of generic types. We can write code that supports a number of types by using a stand in, often just the letter T. This includes when defining structures and enumerations.

Rust does not have higher-kinded types. If you don't know what that means, don't worry about it. If you do, then you might be looking for them. You can stop.

