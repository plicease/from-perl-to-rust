# Pattern Matching

Rust also has `match` which is like match from many functional programming languages. It is very much dependent on types. Perl's attempt at a switch syntax with smart-matching is perhaps the closest analogy. Indeed, I think smart-matching failed because it tried to use information about the types of the operands and it didn't really have this information. Unlike every other operator in Perl, the smart-match operator was not the boss.

**Update** (2022-05-29): [I talked about pattern matching](https://www.youtube.com/watch?v=STrsB_hyP_A&t=3015s) at the Rust & C++ Cardiff book club. We are reading [The Rust Programming Language](https://doc.rust-lang.org/book/) together and I did the bit on chapter 18 (it starts about 50 minutes in). Also, [you can view my slides](https://oylenshpeegul.github.io/slides/pattern-matching/).


## Regular Expressions

Perl has built in regular expressions. Rust has a `regex` package that must be imported. Rust regexes are like Perl's `re::engine::RE2` regexes, not its built-ins.
